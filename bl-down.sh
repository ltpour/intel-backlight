#!/bin/sh
bl_device=/sys/class/backlight/intel_backlight/brightness
if [ $(($(cat $bl_device))) -gt 400 ]; then
    echo $(($(cat $bl_device)-$(($(cat $bl_device)/2)))) | sudo tee $bl_device
fi
