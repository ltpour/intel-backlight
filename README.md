# INTEL-BACKLIGHT #

### Summary ###

ACPI actions and events for adjusting Intel backlight with brightness keys.

Works by doubling (BRTUP) or halving (BRTDN) the current /sys/class/backlight/intel_backlight/brightness value.

Assuming /sys/class/backlight/intel_backlight/max_brigthness == 6000, the minimum brightness is hardcoded to 375, as lower values would require dealing with the remainder in integer division.

### Usage ###

Place bl-up and bl-down under /etc/acpi/events/ and bl-up.sh and bl-down.sh under /etc/acpi/actions/.
